Use case
When you have users with a imagefield empty and you want to display an standard avatar (male or female) according the values of another field (sex field)

Steps
1. It is a widget for image field. Please enter a image by default in your field settings. You will not use this image but is necessary to replace it by male or female widget
2. The formatter selects a male or female avatar according selectbox from a sex field
3. You must define four parameters in your display settings
a) Enter a filename for male avatar
b) Enter a filename for female avatar
a) Enter a fieldname for sex field
4. This sex field must be contains two values: male and female.
5. The module include 2 demo images you must put inside File directory of your image field
For example, if your image field has as File directory = news/images you must put these images inside this directory
